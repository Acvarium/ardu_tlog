#include <OneWire.h>
OneWire ds(8);
#include <SPI.h>
#include <SD.h>
#include <ThreeWire.h>  
#include <RtcDS1302.h>
const int chipSelect = 4;
#define countof(a) (sizeof(a) / sizeof(a[0]))
float Temp;
ThreeWire myWire(4,5,2); // IO, SCLK, CE
RtcDS1302<ThreeWire> Rtc(myWire);
void digitalInterrupt(){
  //needed for the digital input interrupt
}

void setup() {
   Serial.begin(9600);
   
  //Save Power by writing all Digital IO LOW - note that pins just need to be tied one way or another, do not damage devices!
  for (int i = 0; i < 20; i++) {
    if(i != 2)//just because the button is hooked up to digital pin 2
    pinMode(i, OUTPUT);
  }
  attachInterrupt(0, digitalInterrupt, FALLING); //interrupt for waking up

  Rtc.Begin();

    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
    Serial.print(__TIME__);
    Serial.print("  ");
    Serial.println(__DATE__);
    if (!Rtc.IsDateTimeValid()) 
    {
        // Common Causes:
        //    1) first time you ran and the device wasn't running yet
        //    2) the battery on the device is low or even missing

        Serial.println("RTC lost confidence in the DateTime!");
        Rtc.SetDateTime(compiled);
    }
    if (Rtc.GetIsWriteProtected())
    {
//        Serial.println("RTC was write protected, enabling writing now");
        Rtc.SetIsWriteProtected(false);
    }

    if (!Rtc.GetIsRunning())
    {
//        Serial.println("RTC was not actively running, starting now");
        Rtc.SetIsRunning(true);
    }

    RtcDateTime now = Rtc.GetDateTime();

    if (now < compiled) 
    {
//        Serial.println("RTC is older than compile time!  (Updating DateTime)");
        Rtc.SetDateTime(compiled);
    }
  
    //SETUP WATCHDOG TIMER
    WDTCSR = (24);//change enable and WDE - also resets
    WDTCSR = (33);//prescalers only - get rid of the WDE and WDCE bit
    WDTCSR |= (1<<6);//enable interrupt mode
    
    //Disable ADC - don't forget to flip back after waking up if using ADC in your application ADCSRA |= (1 << 7);
    ADCSRA &= ~(1 << 7);
    
    //ENABLE SLEEP - this enables the sleep mode
    SMCR |= (1 << 2); //power down mode
    SMCR |= 1;//enable sleep
}

void displayDateTime(const RtcDateTime& dt)
{
  char datestring[20];

    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u:%02u:%02u"),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
   
   Serial.println(datestring);
}

void readTemperature()
{
    byte t_data[2];
    ds.reset(); 
    ds.write(0xCC);
    ds.write(0x44);
    delay(15);
    ds.reset();
    ds.write(0xCC);
    ds.write(0xBE);
    t_data[0] = ds.read(); 
    t_data[1] = ds.read();
    int t = (t_data[1]<< 8)+t_data[0];
    int t_int = t>>4;
    Temp = (float)t_int + (float)(t - (t_int<<4)) * 0.0625; 
}

bool checkSd()
{
    if (!SD.begin(chipSelect)) {
        Serial.println("no card");
        // don't do anything more:
        return false;
    }
    return true;
}

void outputTemp()
{
    if (Temp < 80)
    {
        RtcDateTime now = Rtc.GetDateTime();
        long sec = ((long)now.Hour() * 3600) + ((long)now.Minute() * 60) + (long)now.Second();
        String fileName = "";
        fileName += String(now.Year() - 2000) + String(now.Month()) + String(now.Day()) + ".csv";
        String dataString = "";
        dataString += String(sec);
        dataString += ",";
        dataString += String(now.Hour());
        dataString += ":";
        dataString += String(now.Minute());
        dataString += ":";
        dataString += String(now.Second());
        dataString += ",";
        dataString += String(Temp);     
        File dataFile = SD.open(fileName, FILE_WRITE);
        if (dataFile) 
        {
            dataFile.println(dataString);
            dataFile.close();
            Serial.println(dataString);
        }
    }
}

void loop() {
//  RtcDateTime now = Rtc.GetDateTime();
//  displayDateTime(now);

  if (checkSd() && Rtc.IsDateTimeValid())
  {
      readTemperature();
      outputTemp();
  }
  delay(35);

  for(int i=0; i<8; i++)
  {
//      BOD DISABLE - this must be called right before the __asm__ sleep instruction
      MCUCR |= (3 << 5); //set both BODS and BODSE at the same time
      MCUCR = (MCUCR & ~(1 << 5)) | (1 << 6); //then set the BODS bit and clear the BODSE bit at the same time
      __asm__  __volatile__("sleep");//in line assembler to go to sleep

  }
}

ISR(WDT_vect){
  //DON'T FORGET THIS!  Needed for the watch dog timer.  This is called after a watch dog timer timeout - this is the interrupt function called after waking up
}// watchdog interrupt
